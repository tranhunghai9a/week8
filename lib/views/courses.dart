import 'package:flutter/material.dart';
import 'dart:core';

class Courses extends StatefulWidget {
  static const route = '/courses';
  const Courses({Key? key}) : super(key: key);

  @override
  _CoursesState createState() => _CoursesState();
}

class _CoursesState extends State<Courses> {
  @override
  Widget build(BuildContext context) {
    var args = ModalRoute.of(context)!.settings.arguments as List;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('courses'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
          children: args.map((c) => Text(c.toString())).toList(),
        ),
      ),
    );
  }
}
